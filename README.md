Hi all,

one of my million hobbies is **making a documentary movies** from holidays. **Every time** with new camera content i **spending a weeks** with my iMac and editing,
cutting, exporting, encoding, etc. And **every time** i **don't like developers** from Apple for Final Cut Pro **error messages** because tells me nothing.
In 2018 we was on mototrip in Moroco and in 2019 i found time to editing a video. After **months working** on my new video i **wanted export** and take a look on 4K TV. 
Next few weeks i was trying to read whole internet and looking for help but i don't have a luck. Because Final Cut Pro or Compressor is **unable to export** this movie. It's a long and not funny story.

I found for me and for all of you solution. My total **n00b script** can help with:

1) **find** and **log** corrupted or glitched media in Final Cut Pro library
2) **fix** those media, **backup** originals and **copy clean files** to the Final Cut Pro library
3) **revert** original files from backup
4) or **check** system if script doesn't work


**How can i do this ?**

1) using **ffmpeg** at https://www.ffmpeg.org to analyze and fix
   - h.265 / hevc supported
   - GPU encoding with VideoToolbox at https://developer.apple.com/documentation/videotoolbox
2) using **Homebrew** at https://brew.sh/
3) using few libs from **Python3** at https://www.python.org/download/releases/3.0/
4) and of course my loving shell script with **#!bin/sh**


**How to install ?**

1) in Mac OS X run **terminal**
2) install homebrew "**/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)**"
3) install ffmpeg "**brew install ffmpeg**"
4) install python "**brew install python**"
5) run **Final Cut Pro**, select project and **Export XML** from File in menu
6) download this **repair kit**
7) use repair kit to analyse your project media library "**./laundromat.sh test your_exported_xml_file.fcpxml**"
8) use repair kit to fix error in media library "**./laundromat.sh fix your_exported_xml_file.fcpxml**"
9) use repair kit to revert all changes "**./laundromat.sh revert your_exported_xml_file.fcpxml**"
10) use rpair kit to check system if you have all the laundromat needs "**./laundromat.sh check your_exported_xml_file.fcpxml**"


ENJOY more time with Final Cut Pro not with searching for errors :-)

P.S. If you have more troubles with Final Cut Pro like rotating beach ball, freezeing, crashing etc. repairing of corrupted media solving all your problems with Final Cut Pro maybe ! Try it :-)