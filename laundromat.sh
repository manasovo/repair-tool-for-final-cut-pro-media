#!/bin/sh

# laundromat is simple n00b script to find & celan corupted media files in Final Cut Pro library on Mac OS X
# using ffmpeg, python and bash
# version 0.99 beta

# TODO
# clean check log for fresh analyse 

# MAC OS X KERNEL, PYTHON3, FFMPEG CHECK and Final Cut Pro
DARWIN=`uname`
PYTHON3_EXIST=`which python3`
FF_EXIST=`which ffmpeg`

if [ "$DARWIN" = "Darwin" ]; then

    if [ -e "$PYTHON3_EXIST" ]; then
    
            if [ -e "$FF_EXIST" ]; then
                    echo "\c"
                else
                    echo "\033[1;31mffmpeg not found\033[0m"
                    exit 1
            fi
    
        else
            echo "\033[1;31mpython3 not found\033[0m"
            exit 1
    fi

    else
        echo "\033[1;31mMac OS X Darwin Kernel not found\033[0m"
        exit 1
fi              

FCP_EXIST="/Applications/Final Cut Pro.app"
FCP_VERSION=`mdls -name kMDItemVersion /Applications/Final\ Cut\ Pro.app | cut -d '"' -f2`

# CONFIG
CPU_THREADS=`sysctl hw.logicalcpu | awk '{ print $2 }'`
GPU=`ffmpeg -hwaccels -hide_banner | sed -n 2p`
EXCLUDE_PIC=".png|.jpg|.jpeg|.heic"
DATE=`date +"%Y-%m-%d_%H-%M-%S"`


# GPU SUPPORT
if [ $GPU = 0 ]; then
        GPU_CHECK="0"
    else
        GPU_CHECK="1"
fi


# COMMANDS AND OPTIONS
usage()
{
    cat <<EOF
Usage: ./laundromat.sh <test/fix/revert/check> <export_file.fcpxml>

        test   - load your exported XML file from Final Cut Pro (open project, File and Export XML) and test  
                 every media (video/audio) for errors. Using ffmpeg. All errors are saved in log file.
        fix    - load error log and re-transcoded every error media to the same source quality, then
                 backup origin media and move new fix media back to the library to same place.
        revert - revert all operation (fix media) this means move backup media to the origin place in library
                 totaly seamless with Final Cut Pro.
        check  - check system for future debuging

EOF
    exit
}


# 1.STEP - check parameters
if [ ${#1} = 0 ]; then
    echo "\033[1;31mmissing operation like test/fix/revert/check\033[0m"
    usage
fi

if [ ${#2} = 0 ]; then
    echo "\033[1;31mmissing export_file.fcpxml\033[0m"
    usage
fi


# 3.STEP - testing video & audio files from FCPX library
test()
{

# backup old logs
mv $LOG $LOG.$DATE 2> /dev/null

echo "\033[32mdetected CPU threads\033[0m $CPU_THREADS \c"

# GPU information
if [ $GPU_CHECK = 1 ]; then
    echo "\033[32mand \033[0mGPU\033[32m support\033[0m"
else
    echo ""
fi

# check Final Cut Pro XML version
XML_VERSION=`cat $2 | grep "<fcpxml version" |  sed -e 's@.*version="@@g' -e 's@".*@@g'`
XML_REQUIRED="1.9"
    if [ "$(printf '%s\n' "$XML_REQUIRED" "$XML_VERSION" | sort -V | head -n1)" = "$XML_REQUIRED" ]; then 
        #echo "Greater than or equal to ${XML_REQUIRED}"
        MEDIA="<media-rep"
    else
        #echo "Less than ${XML_REQUIRED}"
        MEDIA="<asset id"
    fi

    for XML in `cat $2 | grep "$MEDIA" | sed -e 's@.*file://@@g' -e 's@".*@@g' | grep -Ev $EXCLUDE_PIC` ;do

    MEDIA_FILE=`echo $XML | sed -e 's@.*/@@g' | python3 -c 'import sys; import urllib.parse; x=sys.stdin.read(); print(urllib.parse.unquote(x))'`
    echo "\033[1;34mtesting\033[0m $MEDIA_FILE \c"
    
    XML_CLEAN=`echo $XML | python3 -c 'import sys; import urllib.parse; x=sys.stdin.read(); print(urllib.parse.unquote(x))'`

    # percentage counter
    counter()
    {
    if [ $? -eq 0 ]; then
            COUNTER=$[$COUNTER +1]
            printf "\033[1;32mdone\033[0m %.f" $(echo "$COUNTER/$LINE*100" | bc -l) 
            echo "%"
            # create check log for good media files
            echo $XML >> $LOG_OK
        else 
            COUNTER=$[$COUNTER +1]
            printf "\033[1;91merror\033[0m %.f" $(echo "$COUNTER/$LINE*100" | bc -l) 
            echo "%" 
            # create error log for corrupted media files
            echo $XML >> $LOG
    fi
    }

    # if Mac have a more core in CPU whole decoding process (not encoding) is faster then use GPU - need to find optimal solution ... today 8 :-)
    CPU_CORES_OPTIMAL=8

    # with GPU decoding few corrupted video pass without problems and Final Cut Pro is OK also :-) We will see.
    # GPU exist and CPU CORES are more than optimal. For decoder using CPU.
    if ([ $GPU_CHECK = 1 ] && [ $CPU_THREADS -ge $CPU_CORES_OPTIMAL ]); then
    echo "\033[1;33mCPU\033[0m \c"
        if [ -e $LOG_OK ]; then  # skipping tested media
            LOG_FOUND=`cat $LOG_OK | grep $XML | wc -l`
                if [ $LOG_FOUND -ge 1 ]; then
                echo "\033[1;32mskipping\033[0m \c"
                counter
                    else  
                    caffeinate ffmpeg -xerror -v error -threads $CPU_THREADS -i "$XML_CLEAN" -f null - 2> /dev/null
                    counter
                fi
            else
            caffeinate ffmpeg -xerror -v error -threads $CPU_THREADS -i "$XML_CLEAN" -f null - 2> /dev/null
            counter
        fi 
    fi
     
    # GPU exist and CPU CORES are less than optimal. For decoder using GPU.   
    if ([ $GPU_CHECK = 1 ] && [ $CPU_THREADS -lt $CPU_CORES_OPTIMAL ]); then
    echo "\033[1;33mGPU\033[0m \c"
        if [ -e $LOG_OK ]; then  # skipping tested media
            LOG_FOUND=`cat $LOG_OK | grep $XML | wc -l`
                if [ $LOG_FOUND -ge 1 ]; then
                echo "\033[1;32mskipping\033[0m \c"
                counter
                    else  
                    caffeinate ffmpeg -xerror -v error -threads $CPU_THREADS -hwaccel $GPU -i "$XML_CLEAN" -f null - 2> /dev/null
                    counter
                fi
            else
            caffeinate ffmpeg -xerror -v error -threads $CPU_THREADS -hwaccel $GPU -i "$XML_CLEAN" -f null - 2> /dev/null
            counter
        fi
    fi
    
    # no GPU       
    if [ $GPU_CHECK = 0 ]; then
    echo "\033[1;33mCPU\033[0m \c"
    
        if [ -e $LOG_OK ]; then  # skipping tested media
            LOG_FOUND=`cat $LOG_OK | grep $XML | wc -l`
                if [ $LOG_FOUND -ge 1 ]; then
                echo "\033[1;32mskipping\033[0m \c"
                counter
                    else  
                    caffeinate ffmpeg -xerror -v error -threads $CPU_THREADS -i "$XML_CLEAN" -f null - 2> /dev/null
                    counter
                fi
            else
            caffeinate ffmpeg -xerror -v error -threads $CPU_THREADS -i "$XML_CLEAN" -f null - 2> /dev/null
            counter
        fi 
    fi
  
done

# optimizing check log for duplicity
echo "\033[32moptimizing log file:\033[0m \c"
cat $LOG_OK | sort | uniq >> $LOG_OK.clean
mv $LOG_OK.clean $LOG_OK
echo "\033[1;32mdone\033[0m"

exit 1
}

# 4.STEP - repair source files with error
fix()
{

# check if logs exists
if [ -e $LOG ]; then
 
    # check if user have a brain :-)
    echo "\033[1;31mare you sure to fix & replace corupted file in your Final Cut Pro library \033[0m(yes/no) ? \c"
    read question
    if [ "$question" = "yes" ]; then
  
    # 5.STEP - do the dangerous stuff
        for FIX_ERROR in `cat $LOG ` ;do
          
            FIX_SOURCE=`echo $FIX_ERROR | python3 -c 'import sys; import urllib.parse; x=sys.stdin.read(); print(urllib.parse.unquote(x))'`
            FIX_BACKUP=`echo $FIX_ERROR | python3 -c 'import sys; import urllib.parse; x=sys.stdin.read(); print(urllib.parse.unquote(x))' | sed 's/\.[^.]*$/_backup_origin&/'`

            #fixing corrupted media file started
            MEDIA_FILE=`echo $FIX_SOURCE | sed -e 's@.*/@@g'`
            echo "\033[1;34mfixing\033[0m $MEDIA_FILE \c"

            # check if file & backup exist
            if [ -e "$FIX_SOURCE" ]; then 

                    if [ -e "$FIX_BACKUP" ]; then
                            echo "\033[0;31mBCK\033[0m \c"
                        else
                            mv -i "$FIX_SOURCE" "$FIX_BACKUP" 2> /dev/null
                            echo "\033[0;32mBCK\033[0m \c"
                    fi

                else
                    echo "\033[0;31mnot found\033[0m"
            fi
            
            # 7.STEP - read codec and bitrate in source media    
            CODEC=`ffprobe "$FIX_BACKUP" -v quiet -hide_banner -show_streams -select_streams v:0 -show_entries stream=codec_name | grep codec_name | sed -e 's@.*=@@g'`
            BITRATE=`ffprobe "$FIX_BACKUP" -v quiet -hide_banner -show_streams -select_streams v:0 -show_entries stream=bit_rate | grep bit_rate | sed -e 's@.*=@@g'`


                    encoding_common()
                    {
                    
                    # double quality jusrt for sure
                    BITRATE_MAX=`printf $(echo "$BITRATE*2" | bc -l)`
                    
                    # common commnad if hevc or h264 not found in source media
                    echo "\033[0;33mCPU common \033[0;32mbps\033[0m $BITRATE_MAX \c"
                    caffeinate ffmpeg -v quiet -hide_banner -threads $CPU_THREADS -i "$FIX_BACKUP" -b:v $BITRATE_MAX -y "$FIX_SOURCE"
                    }


                    encoding_hevc()
                    {                    
                
                    # if sourcemedia is HEVC need to check if ffmpeg have a HEVC encoder 
                    FF_ENC_HEVC=`ffmpeg -hide_banner -encoders | grep hevc | wc -l`

                    if [ "$FF_ENC_HEVC" = 0 ]; then
                            echo "\c"
                        else

                            if [ "$GPU_CHECK" = "1" ]; then

                                    FF_ENC_HEVC=`ffmpeg -hide_banner -encoders | grep $GPU | grep hevc | awk '{ print $2 }'`

                                    echo "\033[0;33mGPU $FF_ENC_HEVC \033[0;32mbps\033[0m $BITRATE \c"
                                    # -tag:v hevc1 is important to create QuickTime Media for Final Cut Pro
                                    caffeinate ffmpeg -v quiet -hide_banner -threads $CPU_THREADS -i "$FIX_BACKUP" -tag:v hvc1 -c:v $FF_ENC_HEVC -b:v $BITRATE -y "$FIX_SOURCE"

                                else

                                    FF_ENC_HEVC=`ffmpeg -hide_banner -encoders | grep hevc | sed -n 1p | awk '{ print $2 }'`

                                    echo "\033[0;33mCPU $FF_ENC_HEVC \033[0;32mbps\033[0m $BITRATE \c"
                                    caffeinate ffmpeg -v quiet -hide_banner -threads $CPU_THREADS -i "$FIX_BACKUP" -tag:v hvc1 -c:v $FF_ENC_HEVC -b:v $BITRATE -y "$FIX_SOURCE"
                            fi
                    fi
                    }


                    encoding_h264()
                    {                    
                                 
                    # if sourcemedia is h.264 need to check if ffmpeg have a h.264 encoder
                    FF_ENC_H264=`ffmpeg -hide_banner -encoders | grep h264 | wc -l`
                   
                    if [ "$FF_ENC_H264" = 0 ]; then
                              echo "\c"
                        else

                            if [ "$GPU_CHECK" = "1" ]; then

                                    FF_ENC_H264=`ffmpeg -hide_banner -encoders | grep $GPU | grep h264 | awk '{ print $2 }'`

                                    echo "\033[0;33mGPU $FF_ENC_H264 \033[0;32mbps\033[0m $BITRATE \c"
                                    caffeinate ffmpeg -v quiet -hide_banner -threads $CPU_THREADS -i "$FIX_BACKUP" -c:v $FF_ENC_H264 -b:v $BITRATE -y "$FIX_SOURCE" 1> /dev/null

                                else

                                    FF_ENC_H264=`ffmpeg -hide_banner -encoders | grep h264 | sed -n 1p | awk '{ print $2 }'`

                                    echo "\033[0;33mCPU $FF_ENC_H264 \033[0;32mbps\033[0m $BITRATE \c"
                                    caffeinate ffmpeg -v quiet -hide_banner -threads $CPU_THREADS -i "$FIX_BACKUP" -c:v $FF_ENC_H264 -b:v $BITRATE -y "$FIX_SOURCE" 1> /dev/null
                            fi
                    fi
                    }


                    # percentage counter
                    counter_fix()
                    {  
                        LINE_FIX=`cat $LOG | wc -l`
                   
                        COUNTER_FIX=$[$COUNTER_FIX +1]
                        printf "\033[1;32mdone\033[0m %.f" $(echo "$COUNTER_FIX/$LINE_FIX*100" | bc -l) 
                        echo "%"
                    }


                    # 8.STEP - specific encoding parametrs for each codec on source media
                    if [ "$CODEC" = "hevc" ]; then
                            echo "\033[0;32m$CODEC\033[0m \c"
                            encoding_hevc
                            counter_fix
                    fi

        
                    if [ "$CODEC" = "h264" ]; then
                            echo "\033[0;32m$CODEC\033[0m \c"
                            encoding_h264
                            counter_fix
                    fi


                    if [ "$CODEC" = "$*" ]; then
                            echo "\033[0;32m$CODEC\033[0m \c"
                            encoding_common
                            counter_fix
                    fi
        done
    
    else
    exit 1                    
    fi

else
echo "\033[1;31merror log doesn't exist. Run first "test".\033[0m \n"
exit 1
fi
}


# 9.STEP - revert original data from backups
revert()
{

if [ -e $LOG ]; then

    echo "\033[1;31mare you sure to revert files from backup \033[0m(yes/no) ? \c"
    read question
    if [ "$question" = "yes" ]; then
   
            for BACKUP in `cat $LOG `; do
            FIX_SOURCE=`echo $BACKUP | python3 -c 'import sys; import urllib.parse; x=sys.stdin.read(); print(urllib.parse.unquote(x))'`
            FIX_BACKUP=`echo $BACKUP | python3 -c 'import sys; import urllib.parse; x=sys.stdin.read(); print(urllib.parse.unquote(x))' | sed 's/\.[^.]*$/_backup_origin&/'`
            
            echo "\033[1;32mrevert backup files ->\033[0m \c"

            if [ -e "$FIX_BACKUP" ]; then
                    echo "$FIX_SOURCE"
                    mv "$FIX_BACKUP" "$FIX_SOURCE"
                else
                    echo "\033[1;31mbackup file not found\033[0m"
            fi

            done

        else
        exit 1
    fi

    else
        echo "\033[1;31merror log doesn't exist. Run first test.\033[0m \n"
        exit 1
fi

}


system_check ()
{   
    if [ "$DARWIN" = "Darwin" ]; then
        echo "\033[0;32mMac OS X Darwin Kernel detected:\033[0m \c"
        uname -r
    else
        echo "\033[1;31mMac OS X Darwin Kernel\033[31m not found\033[0m"
    fi   

    echo "\033[32mCPU threads:\033[0m $CPU_THREADS"
    
    if [ -d "$FCP_EXIST" ]; then
        echo "\033[0;32mFinal Cut Pro version:\033[0m \c"
        echo $FCP_VERSION
    else
        echo "\033[1;31mFinal Cut Pro\033[31m not found\033[0m"
    fi
    
    echo "\033[32mXML version:\033[0m $XML_VERSION"
    echo "\033[32mmedia files in export:\033[0m $LINE"

    BREW_EXIST=`which brew`
    if [ -e "$BREW_EXIST" ]; then
        echo "\033[32mhomebrew installed:\033[0m $BREW_EXIST"
        BREW_VERSION=`brew --version |  sed -n 1p | awk '{ print $2 }'`
        echo "\033[32mhomebrew version:\033[0m $BREW_VERSION"
    else
        echo "\033[32mhomebrew installed:\033[31m not found\033[0m"
    fi
   
    if [ -e "$PYTHON3_EXIST" ]; then
        echo "\033[32mpython3 installed:\033[0m $PYTHON3_EXIST"
        PYTHON3_VERSION=`python3 -V | awk '{ print $2 }'`
        echo "\033[32mpython3 version:\033[0m $PYTHON3_VERSION"
    else
        echo "\033[32mpython3 installed:\033[31m not found\033[0m"
    fi
   
    if [ -e "$FF_EXIST" ]; then
        echo "\033[32mffmpeg installed:\033[0m $FF_EXIST"
        
        FF_VERSION=`ffmpeg -version |  sed -n 1p | awk '{ print $3 }'`
        echo "\033[32mffmpeg version:\033[0m $FF_VERSION"
        
        echo "\033[32mffmpeg GPU:\033[0m $GPU"
        
        FF_ENC_HEVC=`ffmpeg -hide_banner -encoders | grep hevc | wc -l | awk '{print $1}'`
        FF_ENC_HEVC_CODEC=`ffmpeg -hide_banner -encoders | grep hevc | awk '{print $2}' | tr '\n' ' '`
        echo "\033[32mffmpeg h.265/hevc support:\033[0m $FF_ENC_HEVC \033[33m -> $FF_ENC_HEVC_CODEC \033[0m"
        
        FF_ENC_H264=`ffmpeg -hide_banner -encoders | grep h264 | wc -l | awk '{print $1}'`
        FF_ENC_H264_CODEC=`ffmpeg -hide_banner -encoders | grep h264 | awk '{print $2}' | tr '\n' ' '`
        echo "\033[32mffmpeg h.264 support:\033[0m $FF_ENC_H264 \033[33m -> $FF_ENC_H264_CODEC \033[0m"
    else
        echo "\033[32mffmpeg installed:\033[31m not found\033[0m"
    fi
}


XML_FILE=`echo $2`

if [ -e $XML_FILE ]; then
         
        # 2.STEP - check next options and go to the exact place
        LOG=`echo $2 | sed 's/\.fcpxml/\_error.log/'`
        LOG_OK=`echo $2 | sed 's/\.fcpxml/\_check_ok.log/'`

        # check Final Cut Pro XML version
        XML_VERSION=`cat $2 | grep "<fcpxml version" |  sed -e 's@.*version="@@g' -e 's@".*@@g'`
        XML_REQUIRED="1.9"
            if [ "$(printf '%s\n' "$XML_REQUIRED" "$XML_VERSION" | sort -V | head -n1)" = "$XML_REQUIRED" ]; then 
                #echo "Greater than or equal to ${XML_REQUIRED}"
                MEDIA="<media-rep"
            else
                #echo "Less than ${XML_REQUIRED}"
                MEDIA="<asset id"
            fi

        LINE=`cat $2 | grep "$MEDIA" | grep -Ev $EXCLUDE_PIC | wc -l | awk '{print $1}'`

        # check in XML export have a media file 
        if [ $LINE -gt 0 ]; then

                if [ $1 = test ]; then
                    test $1 $2
                fi

                if [ $1 = fix ]; then
                    fix $1 $2
                fi

                if [ $1 = revert ]; then
                    revert $1 $2
                fi

                if [ $1 = check ]; then
                    system_check $1 $2
                fi
                exit 1

            else 
                echo "\033[1;31mno media file found in XML export\033[0m"
        fi

    else
        echo "\033[1;31mXML export file not found\033[0m"
fi
